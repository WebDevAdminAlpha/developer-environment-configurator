# Distribution Developer Environment Configurator

## Overview and Goals

The goal is to install the minimum toolset necessary to have an efficeint
experience as a Distribution Team Developer. This means it will include:

- common configuration snippets
- common shell macros
- cli tools broadly accepted and referenced by the team during work and
  presentations

## Running the Configurator

### Runtime Requirements

The configurator requires:

- Ansible
- Package Management
  - yum
  - dnf
  - apt
  - HomeBrew

### Running the playbook

Clone this repository and run:

```
ansible-playbook workstation.yml --ask-become-pass
```

### Configurable Variables

Override the following variables to control options in the playbooks:

|Variable|Description|
|-|-|
|`helm_tarball_url`|The full url to a helm install tarball.|
|`helm_tarball_checksum`|The corresponding checksum to a helm tarball. Use format `sha256:CHECKSUM` for the value.|
|`user_local_bin`|Defaults to `/usr/local/bin`, set if another location is preferred for user installed binaries.|
|`helm_arch`|Defaults to `linux-amd64`, it will be the directory inside the helm tarball containing **helm** and **tiller**.

## Local Secrets

Shell macros may require secrets in the environment. Add these
secrets to `~.gitlab_tokens` to make use of the macros.

```
# API token for downloading Omnibus Package Builds
OMNIBUS_PACKAGE_TOKEN=[API_TOKEN]
```
